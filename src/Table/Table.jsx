// import * as React from "react";
import {
  Button,
  FormControl,
  InputLabel,
  LinearProgress,
  MenuItem,
  Select,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { ExportJsonCsv } from "react-export-json-csv";

import axios from "axios";

import React, { useEffect, useState } from "react";
import { mkConfig, generateCsv, download } from "export-to-csv";
import { PieChart } from "@mui/x-charts";
import endpoints from "../endpoints";

const columns = [
  { id: "device_name", label: "NAME", minWidth: 50 },
  { id: "created_at", label: "TIMESTAMP", minWidth: 100 },
  { id: "rssi", label: "RSSI (dB)", minWidth: 100 },
  { id: "bridge", label: "HAZARD POINT", minWidth: 100 },
  { id: "distance", label: "Distance (m)", minWidth: 100 },
];

const csv_headers = [
  {
    key: "device_name",
    name: "NAME",
  },
  {
    key: "created_at",
    name: "TIMESTAMP",
  },

  {
    key: "rssi",
    name: "RSSI (db)",
  },

  {
    key: "device_name",
    name: "HAZARD POINT",
  },
  {
    key: "distance",
    name: "DISTANCE(m)",
  },
];

const dataz = [
  {
    id: "1",
    fname: "John",
  },
  {
    id: "2",
    fname: "Doe",
  },
];

export default function DataTable() {
  const [page, setPage] = React.useState(0);
  const [data, setData] = useState([]);
  const [rows, setRows] = useState([]);

  const [graphs, setGraphs] = useState();
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const [loading_graph, setLoadingGraphs] = React.useState(false);
  const [loading_data, setLoadingData] = React.useState(false);

  const [subcategory_data, setSubcategory_Data] = React.useState("ALL");
  const [subcategory_graphs, setSubcategory_Graphs] = React.useState("ALL");

  const menuItems = [
    { value: "ALL", label: "All" },
    { value: "24Hours", label: "LAST 24 HOURS" },
    { value: "January", label: "January" },
    { value: "February", label: "February" },
    { value: "March", label: "March" },
    { value: "April", label: "April" },
    // { value: "INACTIVE", label: "INACTIVE" },
    // { value: "FEATURED", label: "FEATURED" },
  ];

  useEffect(() => {
    // Define the function to make the GET request
    const fetchData = async () => {
     // setLoading(true);
      fetch("https://new-backend-c3hj.onrender.com/get-data", {
        method: "GET",
        headers: {
          // Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json", // optional, depending on your API requirements
        },
      })
        .then((response) => response.json())
        .then((json) => {
          // console.log("JSON:->", json);
          // console.log("customer results:--->", json);
          json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

          setRows(json);

          // Your further processing logic
        })
        .catch((error) => {
          console.error(
            "There was a problem with your fetch operation:",
            error
          );
        })
        .finally(() => {
          //setLoading(false);
        });
    };
    filterGraphData(subcategory_graphs);
    filterTableData(subcategory_graphs);
  }, []); // The empty dependency array ensures that this effect runs once after the initial render

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeType = (event) => {
    console.log("event selected:->", event.target.value);
    setSubcategory_Graphs(event.target.value);

    if (event.target.value) {
      filterGraphData(event.target.value);
    }
  };

  const handleChangeType2 = (event) => {
    console.log("event selected:->", event.target.value);
    setSubcategory_Data(event.target.value);

    if (event.target.value) {
      filterTableData(event.target.value);
    }
  };

  function filterGraphData(filter) {
    console.log("sort services functions called", filter);
    setLoadingGraphs(true);
    // var storedToken = localStorage.getItem("stored_token");
    //var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);
    //
    axios
      //.get(endpoints.getBeautyServices, {
      .get(`${endpoints.filterGraphData}?filter=${filter}`, {
        headers: {
          // Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        if (response.data) {
          const json = response;
          //json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log("Response filtered data: after sorting:--> ");
          //setRows(json);
          console.log("json", json);
          setGraphs(response.data);
        } else {
          // setRows([]);
        }
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        setLoadingGraphs(false);
      });
    /// } else {
    //console.log("Subcategiry empty", subcategory);
    // }
  }

  function filterTableData(filter) {
    console.log("sort services functions called", filter);
    setLoadingData(true);
    // var storedToken = localStorage.getItem("stored_token");
    //var json_token = JSON.parse(storedToken);
    //console.log("json_token", json_token.token);
    //
    axios
      //.get(endpoints.getBeautyServices, {
      .get(`${endpoints.filterTableData}?filter=${filter}`, {
        headers: {
          // Authorization: `Bearer ${json_token.token}`,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        if (response.data) {
          const json = response.data;
          console.log("json", json);
          json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          console.log("Response filtered data: after sorting:--> ");
          //setRows(json);
          console.log("json", json);
          setRows(json);
        } else {
          // setRows([]);
        }
      })
      .catch((error) => {
        console.error("There was a problem with your Axios request:", error);
      })
      .finally(() => {
        setLoadingData(false);
      });
    /// } else {
    //console.log("Subcategiry empty", subcategory);
    // }
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <>
      {" "}
      {loading_data && <LinearProgress />}
      <p>Count:{rows && rows.length}</p>
      <div className="mt-6">
        <FormControl sx={{ m: 1, minWidth: 186 }} size="small">
          <InputLabel id="demo-select-small-label">Filter</InputLabel>
          <Select
            labelId="demo-select-small-label"
            id="subcategory"
            // value={subcategory}
            value={subcategory_data}
            label="subcategory"
            onChange={handleChangeType2}
          >
            {/* Map over the array of menu items to generate MenuItem components */}
            {menuItems.map((item, index) => (
              <MenuItem key={index} value={item.value}>
                {item.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
      <Paper sx={{ width: "100%", overflow: "hidden" }}>
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.ID}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
      {/* <Button variant="contained" onClick={() => download(csvConfig)(csv)}>
        Download csv
      </Button> */}
      <ExportJsonCsv headers={csv_headers} items={rows}>
        <Button variant="contained">Download csv</Button>
      </ExportJsonCsv>
      <div className="mt-6">
        <FormControl sx={{ m: 1, minWidth: 186 }} size="small">
          <InputLabel id="demo-select-small-label">
            Select subcategory
          </InputLabel>
          <Select
            labelId="demo-select-small-label"
            id="subcategory"
            // value={subcategory}
            value={subcategory_graphs}
            label="subcategory"
            onChange={handleChangeType}
          >
            {/* Map over the array of menu items to generate MenuItem components */}
            {menuItems.map((item, index) => (
              <MenuItem key={index} value={item.value}>
                {item.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
   
      {loading_graph && <LinearProgress />}
      <div className="flex flex-row">
        <div className="flex flex-col">
          {" "}
          <PieChart
            series={[
              {
                data: [
                  {
                    id: 0,
                    value: graphs?.tag_1_green_value || 0, // Using optional chaining and providing a default value of 0
                    // label: graphs?.tag_1_green_label || "No Label", // Using optional chaining and providing a default label
                    // label:
                    //   (graphs?.tag_1_green_label || "No Label") +
                    //   ` ${graphs?.tag_1_green_value || 0}`,
                    color: "green",
                  },
                  {
                    id: 1,
                    value: graphs?.tag_1_red_value || 0, // Using optional chaining and providing a default value of 0
                    // label: graphs?.tag_1_red_label || "No Label", // Using optional chaining and providing a default label
                    // label:
                    //   (graphs?.tag_1_red_label || "No Label") +
                    //   ` ${graphs?.tag_1_red_value || 0}`,

                    color: "red",
                  },
                  // { id: 2, value: 20, label: "series C" },
                ],
              },
            ]}
            width={400}
            height={200}
          />
          <span className="flex flex-row gap-1">
            <div className="w-4 h-4 bg-red-600" />
            <p>
              {graphs?.tag_1_red_label}:  {graphs?.tag_1_red_value} hrs
            </p>
          </span>
          <span className="flex flex-row gap-1">
            <div className="w-4 h-4 bg-green-500" />
            <p>
              {graphs?.tag_1_green_label}: {graphs?.tag_1_green_value} hrs
            </p>
          </span>
        </div>

        <div className="flex flex-col">
          {" "}
          <PieChart
            series={[
              {
                data: [
                  {
                    id: 0,
                    value: graphs?.tag_2_green_value || 0, // Using optional chaining and providing a default value of 0
                    // label: graphs?.tag_1_green_label || "No Label", // Using optional chaining and providing a default label
                    // label:
                    //   (graphs?.tag_1_green_label || "No Label") +
                    //   ` ${graphs?.tag_1_green_value || 0}`,
                    color: "green",
                  },
                  {
                    id: 1,
                    value: graphs?.tag_2_red_value || 0, // Using optional chaining and providing a default value of 0
                    // label: graphs?.tag_1_red_label || "No Label", // Using optional chaining and providing a default label
                    // label:
                    //   (graphs?.tag_1_red_label || "No Label") +
                    //   ` ${graphs?.tag_1_red_value || 0}`,

                    color: "red",
                  },
                  // { id: 2, value: 20, label: "series C" },
                ],
              },
            ]}
            width={400}
            height={200}
          />
          <span className="flex flex-row gap-1">
            <div className="w-4 h-4 bg-red-600" />
            <p>
              {graphs?.tag_2_red_label}: {graphs?.tag_2_red_value} hrs
            </p>
          </span>
          <span className="flex flex-row gap-1">
            <div className="w-4 h-4 bg-green-500" />
            <p>
              {graphs?.tag_2_green_label}: {graphs?.tag_2_green_value} hrs
            </p>
          </span>
        </div>

        <div className="flex flex-col">
          {" "}
          <PieChart
            series={[
              {
                data: [
                  {
                    id: 0,
                    value: graphs?.tag_3_green_value || 0, // Using optional chaining and providing a default value of 0
                    // label: graphs?.tag_1_green_label || "No Label", // Using optional chaining and providing a default label
                    // label:
                    //   (graphs?.tag_1_green_label || "No Label") +
                    //   ` ${graphs?.tag_1_green_value || 0}`,
                    color: "green",
                  },
                  {
                    id: 1,
                    value: graphs?.tag_3_red_value || 0, // Using optional chaining and providing a default value of 0
                    // label: graphs?.tag_1_red_label || "No Label", // Using optional chaining and providing a default label
                    // label:
                    //   (graphs?.tag_1_red_label || "No Label") +
                    //   ` ${graphs?.tag_1_red_value || 0}`,

                    color: "red",
                  },
                  // { id: 2, value: 20, label: "series C" },
                ],
              },
            ]}
            width={400}
            height={200}
          />
          <span className="flex flex-row gap-1">
            <div className="w-4 h-4 bg-red-600" />
            <p>
              {graphs?.tag_3_red_label}: {graphs?.tag_3_red_value} hrs
            </p>
          </span>
          <span className="flex flex-row gap-1">
            <div className="w-4 h-4 bg-green-500" />
            <p>
              {graphs?.tag_3_green_label}: {graphs?.tag_3_green_value} hrs
            </p>
          </span>
        </div>

        <div className="flex flex-col">
          {" "}
          <PieChart
            series={[
              {
                data: [
                  {
                    id: 0,
                    value: graphs?.tag_4_green_value || 0, // Using optional chaining and providing a default value of 0
                    // label: graphs?.tag_1_green_label || "No Label", // Using optional chaining and providing a default label
                    // label:
                    //   (graphs?.tag_1_green_label || "No Label") +
                    //   ` ${graphs?.tag_1_green_value || 0}`,
                    color: "green",
                  },
                  {
                    id: 1,
                    value: graphs?.tag_4_red_value || 0, // Using optional chaining and providing a default value of 0
                    // label: graphs?.tag_1_red_label || "No Label", // Using optional chaining and providing a default label
                    // label:
                    //   (graphs?.tag_1_red_label || "No Label") +
                    //   ` ${graphs?.tag_1_red_value || 0}`,

                    color: "red",
                  },
                  // { id: 2, value: 20, label: "series C" },
                ],
              },
            ]}
            width={400}
            height={200}
          />
          <span className="flex flex-row gap-1">
            <div className="w-4 h-4 bg-red-600" />
            <p>
              {graphs?.tag_4_red_label}: {graphs?.tag_4_red_value} hrs
            </p>
          </span>
          <span className="flex flex-row gap-1">
            <div className="w-4 h-4 bg-green-500" />
            <p>
              {graphs?.tag_4_green_label}: {graphs?.tag_4_green_value} hrs
            </p>
          </span>
        </div>
      </div>
    </>
  );
}
