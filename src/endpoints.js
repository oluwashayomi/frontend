//const BASE_URL = "http://localhost:8080";
const BASE_URL= "https://new-backend-c3hj.onrender.com"

const endpoints = {
  filterGraphData: `${BASE_URL}/filter-graphs`,
  filterTableData: `${BASE_URL}/filter-table-data`,
};

export default endpoints;
